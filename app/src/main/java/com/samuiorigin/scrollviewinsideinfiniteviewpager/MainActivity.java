package com.samuiorigin.scrollviewinsideinfiniteviewpager;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    VPAdapter adapter;
    int[] numbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.vp);

        numbers = new int[] {1, 2, 3};

        adapter = new VPAdapter(this, numbers);

        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(1, false);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    if (viewPager.getCurrentItem() == 0) {
                        adapter.previous();
                        viewPager.setCurrentItem(1, false);
                    } else if (viewPager.getCurrentItem() == 2) {
                        adapter.next();
                        viewPager.setCurrentItem(1, false);
                    }
                }
            }
        });
    }
}
