package com.samuiorigin.scrollviewinsideinfiniteviewpager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

public class VPAdapter extends PagerAdapter {

    Context context;
    int[] numbers;

    private LayoutInflater inflater;

    View[] views;
    int action; // -1: previous, 1: next

    // Hold scroll state
    private int allScrollY;
    private ScrollView[] scrollViews;

    public VPAdapter(Context context, int[] numbers) {
        this.context = context;
        this.numbers = numbers;

        inflater = LayoutInflater.from(context);

        views = new View[3];

        // Hold scroll state
        allScrollY = 0;
        scrollViews = new ScrollView[3];
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return o == view;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (action == -1) {
            if (position == 0) {
                View view = inflater.inflate(R.layout.item, container, false);
                ((TextView) view.findViewById(R.id.tvNumber)).setText(numbers[position] + "");
                final ScrollView scrollView = view.findViewById(R.id.sv);
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.setScrollY(allScrollY);
                    }
                });
                scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                        allScrollY = scrollY;
                        syncScrollState();
                    }
                });

                views[2] = views[1]; scrollViews[2] = scrollViews[1];
                views[1] = views[0]; scrollViews[1] = scrollViews[0];
                views[0] = view; scrollViews[0] = scrollView;

                container.addView(view);
            }
        } else if (action == 1) {
            if (position == 2) {
                View view = inflater.inflate(R.layout.item, container, false);
                ((TextView) view.findViewById(R.id.tvNumber)).setText(numbers[position] + "");
                final ScrollView scrollView = view.findViewById(R.id.sv);
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.setScrollY(allScrollY);
                    }
                });
                scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                        allScrollY = scrollY;
                        syncScrollState();
                    }
                });

                views[0] = views[1]; scrollViews[0] = scrollViews[1];
                views[1] = views[2]; scrollViews[1] = scrollViews[2];
                views[2] = view; scrollViews[2] = scrollView;

                container.addView(view);
            }
        } else {
            View view = inflater.inflate(R.layout.item, container, false);
            ((TextView) view.findViewById(R.id.tvNumber)).setText(numbers[position] + "");
            final ScrollView scrollView = view.findViewById(R.id.sv);
            view.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.setScrollY(allScrollY);
                }
            });
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    allScrollY = scrollY;
                    syncScrollState();
                }
            });
            container.addView(view);
            views[position] = view;
            scrollViews[position] = scrollView;
        }
        return views[position];
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (action == -1 && position == 2) {
            container.removeView((View) object);
        } else if (action == 1 && position == 0) {
            container.removeView((View) object);
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    //
    public void previous() {
        numbers[0]--;
        numbers[1]--;
        numbers[2]--;
        action = -1;
        notifyDataSetChanged();
        //delete view 2
        //view 2 = view 1
        //view 1 = view 0
        //view 0 = previous
    }
    public void next() {
        numbers[0]++;
        numbers[1]++;
        numbers[2]++;
        action = 1;
        notifyDataSetChanged();
    }
    public void syncScrollState() {
        scrollViews[0].post(new Runnable() {
            @Override
            public void run() {
                scrollViews[0].setScrollY(scrollViews[1].getScrollY());
            }
        });
        scrollViews[2].post(new Runnable() {
            @Override
            public void run() {
                scrollViews[2].setScrollY(scrollViews[1].getScrollY());
            }
        });
    }
}
